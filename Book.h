#ifndef _BOOK_H_
#define _BOOK_H_
#include <iostream>

/*
 Book.h
M00696479
Created:26/4/2021
Updated:28/4/2021
*/

class Book
{
private:
  std::string author;
  std::string ISBN;
  std::string title;
  int qty;

public:
  Book();
  Book(std::string title, std::string author, std::string ISBN, int qty);

  std::string getISBN();
  int getQty();
  void setQty(int);
  void reduceQty();
  std::string getAuthor();
  std::string getTitle();

  void Display();

  std::string writeTsv();
};
#endif