#ifndef _UTIL_HPP_
#define _UTIL_HPP_

#include <optional>
#include <vector>
/*
 util.h
M00696479
Created:26/4/2021
Updated:28/4/2021
 */

template <typename K, typename V>
class LinearProbingHash
{
private:
    std::optional<K> *key_list;
    V *values;
    size_t qty;
    size_t capacity;

public:
    LinearProbingHash()
    {
        capacity = 100;
        qty = 0;
        key_list = new std::optional<K>[capacity];
        values = new V[capacity];
        for (size_t i = 0; i < capacity; ++i)
        {
            key_list[i] = {};
            values[i] = V();
        }
    }

    ~LinearProbingHash()
    {
        delete[] key_list;
        delete[] values;
    }

    void clear()
    {
        for (size_t i = 0; i < capacity; ++i)
        {
            key_list[i] = {};
            values[i] = V();
        }
        this->qty = 0;
    }

    size_t size()
    {
        return qty;
    }

    bool empty()
    {
        return qty == 0;
    }

    bool contains(K key)
    {
        return key_list[pos(key)].has_value();
    }

    size_t pos(K key)
    {
        size_t index = std::hash<K>{}(key) % capacity;
        while (key_list[index].has_value() && *key_list[index] != key)
            index = (index + 1) % capacity;
        return index;
    }

    void insert(K key, V value)
    {
        size_t index = pos(key);

        if (!key_list[index].has_value())
        {
            key_list[index] = std::optional<K>{key};
            ++qty;
        }

        values[index] = value;

        if (qty >= capacity / 2)
            resize(2 * capacity);
    }

    V get(K key)
    {
        size_t index = pos(key);
        return values[index];
    }

    V &operator[](K key)
    {
        if (!contains(key))
             insert(key, V());

        size_t index = pos(key);
        return values[index];
    }

    void resize(size_t newsize)
    {
        std::optional<K> *oldkeys = key_list;
        V *oldvalues = values;
        size_t old_capacity = capacity;

        key_list = new std::optional<K>[newsize];
        values = new V[newsize];
        for (size_t i = 0; i < newsize; ++i)
        {
            key_list[i] = {};
            values[i] = V();
        }

        capacity = newsize;
        qty = 0;

        for (size_t i = 0; i < old_capacity; ++i)
        {
            if (oldkeys[i].has_value())
                insert(*oldkeys[i], oldvalues[i]);
        }

        delete[] oldkeys;
        delete[] oldvalues;
    }

    void remove(K key)
    {
        size_t index = pos(key);
        if (!key_list[index].has_value())
            return;

        // remove this item
        key_list[index] = {};
        values[index] = V();
        --qty;

        // rehash and reinsert any items in a cluster with the deleted item
        for (size_t i = (index + 1) % capacity; key_list[i].has_value();
             i = (i + 1) % capacity)
        {
            K rekey = *key_list[i];
            V revalue = values[i];
            --qty;

            insert(rekey, revalue);
        }

        // if capacity is too large halve it
        if (qty > 0 && qty <= capacity / 8)
            resize(capacity / 2);
    }

    std::vector<K> Keys()
    {
        std::vector<K> thekeys;
        for (size_t i = 0; i < capacity; ++i)
            if (key_list[i].has_value())
                thekeys.push_back(*key_list[i]);
        return thekeys;
    }
};

template <typename T>
class vec
{
protected:
    T *internal_buffer;
    size_t length;
    size_t capacity;
    void grow();

public:
    vec();
    vec(std::initializer_list<T> init_list);
    ~vec();

    T *data();
    const T *begin() const;
    const T *end() const;

    bool empty();
    size_t size() const;
    void reserve(size_t space);

    void push_back(const T &value);
    void push_front(const T &value);
    T pop_back();
    T pop_front();

    std::optional<size_t> find(const T &search_value);
    void remove(int index);
    void clear();

    T &operator[](int index) const;
};

template <typename T>
vec<T>::vec() : internal_buffer(nullptr)
{
    clear();
}

template <typename T>
vec<T>::vec(std::initializer_list<T> init_list) : internal_buffer(nullptr)
{
    const T *init_list_ptr = init_list.begin();

    length = init_list.size();
    capacity = length;
    internal_buffer = new T[capacity];

    for (size_t i = 0; i < length; i++)
    {
        internal_buffer[i] = init_list_ptr[i];
    }
}

template <typename T>
vec<T>::~vec()
{
    delete[] internal_buffer;
}

template <typename T>
size_t vec<T>::size() const
{
    return length;
}

template <typename T>
bool vec<T>::empty()
{
    return size() == 0;
}

template <typename T>
T *vec<T>::data()
{
    return internal_buffer;
}

template <typename T>
const T *vec<T>::begin() const
{
    return internal_buffer;
}

template <typename T>
const T *vec<T>::end() const
{
    return internal_buffer + size();
}

template <typename T>
void vec<T>::grow()
{
    capacity = capacity * 2;

    T *new_buffer = new T[capacity];
    for (size_t i = 0; i < length; i++)
    {
        new_buffer[i] = internal_buffer[i];
    }

    delete[] internal_buffer;
    internal_buffer = new_buffer;
}

template <typename T>
void vec<T>::push_back(const T &value)
{
    // not enough space
    if (capacity < length + 1)
        grow();

    internal_buffer[length++] = value;
}

template <typename T>
void vec<T>::push_front(const T &value)
{
    // not enough space
    if (capacity < length + 1)
        grow();

    // move all elements one step forward, starting from 0 to free up index 0
    for (size_t i = 0; i < length; i++)
    {
        internal_buffer[i + 1] = internal_buffer[i];
    }

    internal_buffer[0] = value;
}

template <typename T>
T vec<T>::pop_back()
{
    return internal_buffer[--length];
}

template <typename T>
std::optional<size_t> vec<T>::find(const T &search_value)
{
    /*
        naive O(n) search as we cannot make any assumptions
        about T & whether the `vec` is sorted or not.
    */
    for (size_t i = 0; i < length; i++)
    {
        if (internal_buffer[i] == search_value)
        {
            return i;
        }
    }

    return std::nullopt;
}

template <typename T>
void vec<T>::remove(int index)
{
    if (index < 0 || (size_t)index >= length)
    {
        // no-op if index is invalid
        return;
    }

    length--;

    // nothing more to be done
    if (index == length)
        return;

    // move all elements one index back, starting from the provided index
    // effectively erasing the element at pprovided index
    for (size_t i = index; i < length; i++)
    {
        internal_buffer[i] = internal_buffer[i + 1];
    }
}

template <typename T>
void vec<T>::reserve(size_t space)
{
    while (capacity < space)
    {
        grow();
    }

    for (size_t i = 0; i < space; i++)
    {
        internal_buffer[i] = T();
    }
}

template <typename T>
void vec<T>::clear()
{
    length = 0;
    capacity = 8;
    delete[] internal_buffer;
    internal_buffer = new T[capacity];
}

template <typename T>
T &vec<T>::operator[](int index) const
{
    return internal_buffer[index];
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const vec<T> &list)
{
    os << '[';

    size_t length = list.size();
    for (size_t i = 0; i < length; i++)
    {
        os << list[i];
        if (i != length - 1)
            os << ", ";
    }

    os << ']';

    return os;
}

#endif
